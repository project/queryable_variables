// $Id:
What is this module?
   First and foremost, this module is for MODULE DEVELOPERS and not regular drupal UI users that use CCK/Views.  This module
   combines 2 of the most well known features in drupal into 1 usable feature geared towards developers.  These are CCK Fields and
   Drupal's variable_get() etc functions.  I could have very well included views into the fray but I do not think views is up to the
   task of manipulating a fourth normal form table and thus I've only added very limited Views functionality to the module.  It can 
   display fields but I have left out the filters portion.  If someone wants to contribute a filters module to give it some more
   brains with views then they should have at it.  Its beyond the scope of the current project that I needed it for at this time.
   
Is this module for me?
   If you are a module developer then I would suggest getting to know this module well because you would benefit from it in ways you
   could never imagine.  If you are a non-php type developer and dont really mess with PHP code, then just skip this module unless
   a module you are trying to install is telling you that you need to install it first.  It's more of a backend module than a front
   end UI module.
   
Views and CCK are awesome, whats the point of this module?
   Well first if you are using CCK/Views, you are likely using the UI which was developed with non-technical people in mind.  This
   module does the exact same thing but is NOT for use in a UI.  It can be adapted to do so but you would be defeating the purpose I 
   intended it for.  The same holds true with CCK/Views.  If you try to use a module to muck with it, you are defeating the purpose 
   of using them in the first place.  CCK is great for adding fields to nodes using the front end UI but it's almost impossible to
   modify them from modules without some really hard to understand code to the point where its almost unreadable.  This module gives
   the developer the ability to add fields to a node just like with CCK but without all the hassles you encounter with CCK. Imagine
   being able to create/delete/modify CCK variables using Drupal's variable() function set in your module.  Well this is exactly
   what this module does (without actually using CCK, you dont even need CCK/Views installed to run this module).  This module in no
   way replaces CCK nor the Drupal variable system, it doesnt even use them at all.  I actually encourage people to use those first
   and only use this module when you need to store huge amounts of variables associated with nodes in a module using 1 line and
   without the need to create your own tables.  I think a good analogy would be a snowmobile and 4 wheeler.  One is good in snow,
   the other isnt.  But they both get you from a to b just the same no matter what type of terrain you are going over.  The same
   holds true for module development.  In some situations, CCK is like trying to drive a 4 wheeler in the snow, its hard and you get
   stuck a lot.  I consider my module to be the snowmobile that developers have been needing for a very long time including myself. 
   The most common problem with CCK is when you are trying to add a field to a node that is hidden from forms.  Like a counter of
   some sort.  A counter is not something that should be in a form but it is a field that should be connected to the node somehow. 
   You can do this with CCK but it's a pain.  I think this is the simplest usage example I can think of where using this module over
   CCK would make the most sense.  So you should use CCK for all your user based fields still but to add hidden or system only 
   fields to a node, this module would be the best option.  So if you combine CCK and Queryable Variables, your life as a developer
   will become just that much easier.  Some people will ask why not just use drupal's variable system and place the NID right in the
   field name to be stored?  Well the best answer to that is, great.  Go ahead and try to query your variable with mysql to pull out
   the value stored in that fieldname and see what happens.  To do so with drupals variable system you have to load the variable
   first and then try to use PHP to do the sorting, filtering, etc.  Fine and dandy but Mysql is much better suited to do those
   types of tasks and is faster at doing it too.
   
What is the biggest advantage of using this module over drupal's variable system?
   The absolute deal closer is the native support of real mysql queries for improved query times but that isnt something you can 
   really notice right away, so the next biggest advantage is native support for nid/uid/tid/vid associations.  You can associate
   any variable you store with a nid/tid/vid/uid.  Let's say you want to create a quickie shopping cart and dont want to
   re-create something like Zencart but you dont like any of the existing quickie carts.  Well using this module you can do this in
   about 3-4 lines of code and not suffer the performance hit you would suffer if you tried to do the same thing with drupals'
   variable system because it was not designed to hold huge amounts of data in the first place.  This isnt something I expect you to
   go out and do, I just use it as an example and nothing more.  
   Take the following as an example:
   
   $cart = array('item1', 'item2', 'item3');
   queryable_variables_set('shopping_cart', $cart, 'uid', $user->uid);
   
   Ok, no big deal right?  You could have used drupals variable system to do the same thing with this:
   $cart = array('item1', 'item2', 'item3');
   variable_set("shopping_cart_uid_{$user->uid}", $cart);
   
   So whats the difference?  Well with queryable_variables you can then query to see whats in the cart without loading the whole
   thing.  But why would you do that if you could just load up the cart with drupal's variable_get and do the same thing.  Well
   here's the million dollar answer.  Try loading something out of the variables table when it has 40000000 rows that are not
   indexed properly because you had to use a strange field name of shopping_cart_uid_% and it's the ONLY indexed field. This is not
   the best method of storing huge amounts of data and I believe the variables table was never meant to do this anyways that is why
   it's table was not designed with optimal usage in mind.  It was designed to be a small table to begin with. With the new 
   Queryable Variables table you can do REAL mysql queries and have a HUGE table and still pull that cart out in 0.15 seconds as
   opposed to 5-20 seconds with drupal's variable table (depending on how much is in it).  But the next question is if I have to make
   a query, then why I dont I just make my own table in the first place?  Well that answer is just as easy to answer.  The query is
   created for you automatically just by using queryable_variables_get()!!  So things really ARE just as easy as with drupal's
   variable system only you dont get the huge resource usage.  So here is the final example that puts the nail in the coffin for the
   well intentioned variables system:
   
   variable_get("shopping_cart_uid_{$uid}", array());   <--Slow query ~5 secs with large variable tables
   queryable_variables_get("shopping_cart", 'uid', $uid, 'element_value ASC');  <-- FAST query of about ~0.15 secs PLUS it sorted our array by it's array values!! Smokin deal!
   
   The queryable_variables_get() function has many features that you simply can and will never get using variable_get().
   
Ok lets recap what we've gained by using this module:
   A new variables() system that really has no restriction on it's table size that will impact the server CPU usage.  You can have a
   HUGE queryable_variables table with millions of rows and still get reasonable query times with it using just the 3 functions of
   get/set/del without ever having to do a real query yourself to improve performance.  You also get a new table writtin in fourth
   normal form that can be manipulated with queries other than just using the 3 get/set/del functions.  You can use it however you 
   want and you wont impact the queryable_variables() functions because their variables are stored with a special key (element_name
   is always set to php_data_type for set/get variables, something you would never use anyways. It looks for this key on every call
   it makes and thus it can tell if it's data that was stored using the get/set functions or if it's something you put there
   manually.  You also get some new functionality in the get/set/del functions such as sorting both PHP arrays AND OBJECTS.  Thats a 
   wierd one probably because you've never heard of sorting a PHP Object.  But believe me, it's pretty cool to do and is easy with
   the queryable_variables_get() function because it can sort both Arrays AND Objects by either the element name or the element value
   which is something you could previosly only do with PHP arrays using natsort or some other method.  The reason this is possible is
   because we are NOT using PHP to do the sorting in the first place.  Like I said before, Mysql is the master of sorting ;)  Another
   feature you will gain is a new PHP Data type called "Date".  PHP didnt have anything like this before but this module creates
   one and gives you a new function called is_date().  You can then create strings in the form of "2008-04-20 23:10:00" or
   "2008-04-20" and then use the is_date() function and it would return true for both of those strings.  They are also sortable in
   your variables.  So if you make a date using this format in your variable and use DATE to replace element_value in your sorting
   you will be able to sort things by date.  This is possible because all date fields are stored in a mysql date field to begin with.
   
   So when you do: 
   
   queryable_variables_set('somedatename', '2008-04-20') or 
   quaryable_variables('arraynamehere', array('somedatename' => '2008-04-20'))
   
   it doesnt save the value in a string field because it knows that it's a date because the set function actually uses this new
   is_date() function to check for dates and knows to put them in the date column.  It does this for booleans, floats, and strings,
   putting them in the appropriate mysql field types for the so that when it goes to fetch those values in the future it will use
   the fastest query possible.
   
   There is another feature that you cant really see in these examples and that is the ability to timestamp when a variable was
   saved.  Whenever the queryable_variables_set() function is called, it stores a date telling you when the variable was saved.
   You can retrieve this date with the following line:
   queryable_variables_get_date('somedatename');  <-- tells you the date when somedatename variable was saved to the DB
   
   The date feature is not something I actually use myself but it is something I thought would be interesting so I threw it in since
   it is just to easy to do anyways.  So why not, it's cool right :D  Maybe someday I'll find a use for it.
   
What are the limitations of this thing?  I mean come on, it cant be ALL THAT can it?
   Ok Ok, so yes there is a limitation.  Currently only the first level objects/arrays are stored in unserialized form in the DB.
   Any levels after the first are simply serialized just like the drupal variables are.  It's best described in an example:
   
   $test->this = 'hi';  <-- Stored just fine in unserialized form
   $test->something->else = 'oh no!';   <--  This whole element is serialized since the 1st level does not contain a searchable value.
     
   So on the above example you could not do a manual query to pull out the 'oh no' value like you could do with 'hi'.  But heres the
   kicker.  You dont lose any performance because its serialized!  This is because the nid and the field_name are STILL in
   unserialized form and BOTH are indexed.  This gives mysql enough to work with to still perform a very super fast query.  The only
   thing you couldnt do is sorting based on the element_value because it's obviously serialized and that wouldnt work so well
   although it would do it's best to try.  So it really isnt all that big of a limitation anyways because if you were to grab that
   variable, you would never know it was serialized anyways and you would see no performance hit either.  Oh, and if for some reason
   you saved the variable without associating it with a nid/uid/etc then you would notice some slight performance degridation but not
   nearly as much as with the variables table.  It's best to use this table in association with uid/nid etc for best performance but
   it can do fairly well even without those associations.
   
   
   So there you have it, queryable_variables in a nutshell, ok a novelshell.  If you play with this system enough you will begin to
   wonder how you ever lived without it.  It's really great for developers and can be an extremely powerful tool in the right hands.
   The aim of this module is to help novice developers do things very easily by giving them the easy to use get/set/del functions
   without worrying if they are going to blow up the server with slow query times but at the same time it also gives advanced
   developers a very high end system that they can really make some serious apps with.  If you really study the table structure hard
   enough, you would realize that this 1 table could literally replace every single table in drupal without adding a single extra 
   field.  It really is that flexible.  This table can handle ANYTHING you through at it no matter what it is.  It can handle simple
   4 field addresses or you could even make a whole new CMS with it right within drupal.  The sky is the limit, literally.
   
WARNING:  Yes this is an amazing tool.  But like all things amazing, there are some dangers involved.  IF you decide to NOT use the
          3 get/set/del functions and opt to try to do the queries yourself, then you better know what you are doing because this
          table is very flexible but if you screw up a query, you could cause A LOT of CPU usage.  So be careful when constructing
          queries for this table structure.  If you dont know what fourth normal form queries are, then I suggest you dont even
          attempt it.  It would just be asking for trouble.  Let the mysql DBA's show you how to construct queries for this type of
          table or you could cause more slowness than even the drupal variables table.
   
   
      
Function documentation and examples.  
   
   Quickstart Guide, just give me enough to get started!
   Example Usages:

   //Create a variable of some sort to store in the database
   $test['test'] = TRUE;
   
   //Save the variable to the DB and associate it with NID 4 (Remember NID can really be any number included UID etc)
   queryable_variables_set('tests', $test, 4);
   
   //Unset the variable so that we know it's completely empty
   unset($test);
   
   //Re-Load the variable from the DB
   $test = queryable_variables_get('tests', 4);
   
   //We can see the date the variable was tossed into the DB with this command
   echo queryable_variables_get_date('tests', 4);
   
   //And here we can print out what we actually got from the DB
   print_r($test);

DOCS:

Example usages and explainations of each function:


DEFINITION:
   function queryable_variables_get($field_name, $id_field = 'nid', $nid = 0, $orderby = '');
   
   This function grabs a variable from the DB and can optionally sort it based on ANY field in the queryable_variables table.  It also unserializes any serialized arrays/objects as needed.
   
   $field_name = The name of the variable stored in the DB.  Every variable you get from the DB needs to have this.
   
   $id_field = This can be one of uid/nid/tid/vid or you can add new id fields to the table structure and use those as well.
   
   $nid = This is the value of the nid/uid/tid/vid that you specified with $id_field.  If you specify id_field, you have to specify this as well otherwise it associates with ID 0 of whatever nid/uid/etc table name you specified which probably isnt the desired action.
   
   $orderby = This is a real Mysql ORDER BY command.  It can be any of the fields in the DB but the most common will probably be $orderby = 'element_value ASC'

RETURN VALUES:
   Mixed.  Can return any PHP Data type.
   
EXAMPLES:
   function queryable_variables_get('someVariableName');  
   This is the simplest method.  This gets a variable called someVariableName that is associated with NID 0 which is essentially nothing.  There is no nid 0 which means that it's a stand alone variable that isnt really associated with much of anything.
   
   queryable_variables_get('someVariableName', 'nid', 12);
   This is the next simplest method.  This tells the module to grab the someVariableName that is associated with nid 12.  If you use this method then you can have multiple instances of someVariableName BUT each one has to be associated with a different nid.  So in otherwords it's still a unique variable name just like the previous example because the nid 12 makes it unique.  If you were to try to save the the variable twice, it deletes the first one and makes a new one.
   
   queryable_variables_get('someVariableName', 'nid', 12, 'element_name ASC');
   This is almost as complex as it gets.  It's the same thing as above with 1 exception.  IF the returned value is either an object or array then it will sort the object/array by it's element_names in ascending order.
   
   queryable_variables_get('someVariableName', 'nid', 12, 'element_name, element_value ASC');
   And finally a complex one but still not the most complex that is possible.  This one is identical to the previous example with the exception that this one will first sort by element_name and then within that it sorts it even more by element_value.  This is best described in the Mysql manual because this is in fact a real Mysql ORDER BY statement that you are creating.


DEFINITION:
   function queryable_variables_set($field_name, $field_value, $id_field = 'nid', $nid = 0)
   
   This function deletes the old variable of $field_name that was associated either with nid=0 or nid=xxx where nid can be one of nid/uid/etc and xxx is the ID of the user/node/whatever you want it to be associated with.  After that it then saves the new variable to the DB.  This means that every variable in the table is in fact completely unique.  There are no duplicates of any variable in the DB because they always get deleted first and saved second.  Hopefully that wasnt to confusing.  Just thing of it as drupal's variable_set() function, drupal's set function does the same thing.
   
   $field_name = The name of the variable to be stored in the DB.  Every variable you put into the DB needs to have this.
   
   $field_value = This is the actual value to be stored in the DB.  It can be ANY PHP Data type except for Resource.  We dont need to be storing resources hehe.  Other than that it can be anything like object/array/bool/etc.  There is 1 extra PHP Data Type that may be unknown to you that we can store here.  That is the DATE data type.  PHP Doesnt actually have a type called date but this module defines one and uses is_date() to tell if the incoming value is a date.  The is_date() function is also not a PHP function but is defined by this module.  It is documented in the is_date() function of this manual.
   
   $id_field = This can be one of uid/nid/tid/vid or you can add new id fields to the table structure and use those as well.
   
   $nid = This is the value of the nid/uid/tid/vid that you specified with $id_field.  If you specify id_field, you have to specify this as well otherwise it associates with ID 0 of whatever nid/uid/etc table name you specified which probably isnt the desired action.

RETURN VALUES:
   This returns the ID of the variable that was stored in the DB.  
      NOTE: While this function DOES run delete, but since delete returns the ID of the deleted item, we can then reuse that ID and keep auto-incremented ID's to a minimum.
   
EXAMPLES:
   queryable_variables_set('varname', 'its a value!');
   This is the simplest method.  It dumps varname to the DB with a value of 'its a value!' and associates it with nid=0 which is essentionally nothing.  Pretty simple stuff.
   
   queryable_variables_set('varname', 'its a value!', 'uid', 23);
   This is a very useful one, its identical to the above with 1 exception.  This one is associated with UID 23.  This gives you the ability to give users lots of extra fields without actually adding any tables and associating them or any of that jazz.  uid can be any of uid/nid/tid/vid.
   
   queryable_variables_set('varname', 'its a value!', 'nid', 23);
   This is a very useful one, its identical to the above with 1 exception.  This one is associated with NID 23.  This one would NOT overwrite the one in the previous example because nid is different than uid even though the name of the variable and the ID number are the same.  The nid sets it apart and makes it unique.
   
   queryable_variables_set('varname', array("this" => "has a value", "thisone" => "has a value too"), 'uid', 23);
   This is a very useful one, its identical to the above with 1 exception.  This one is an array and each element is stored in the datebase in the element_name and element_value fields.
   
   $id = queryable_variables_set('varname', array("this" => array("has a value", "thisone" => "has a value too")), 'uid', 23);
   This is a very useful one, its identical to the above with 1 exception.  This one is a nested array and each element is NOT stored in the datebase in the element_name and element_value fields because thats not possible.  Instead the whole thing is serialized and placed in element_value.  element_name is set to 'this' and it's value would be a serialized version of the nested array.  The same thing goes for objects.  Only the first level of both objects and arrays are stored in unserialized fashion.  This one also captures the ID which the table gave to the variable.
   
DEFINITION:
   function queryable_variables_del($field_name, $id_field = 'nid', $nid = 0)
   AHHHH! DONT PRESS THE RED BUTTON! NOooooooooooooooooooo.  opps.  its gone.  Guess what this function does ;)
   
   $field_name = The name of the variable in the DB to be nuked.
   
   $id_field = This can be one of uid/nid/tid/vid or you can add new id fields to the table structure and use those as well.
   
   $nid = This is the value of the nid/uid/tid/vid that you specified with $id_field.  If you specify id_field, you have to specify this as well otherwise it associates with ID 0 of whatever nid/uid/etc table name you specified which probably isnt the desired action.

RETURN VALUE:
   This function actually returns the ID of the deleted variable.  Pretty useless right?  NO!!  It's actually pretty useful as it lets us re-use that value if we store it somewhere.  The queryable_variables_set() function is one example of such usage.
   
EXAMPLES:
   queryable_variables_del('thisvariable');
   This nukes the 'thisvariable' that is associated with nid=0 just like it's get/set siblings
   
   queryable_variables_del('thisvariable', 'nid', 14);
   Flush 'thisvariable' that is associated with nid=14 down the toilet.
   
DEFINITION:
    function queryable_variables_get_date($field_name, $id_field = 'nid', $nid = 0)
    This gets the date in which $field_name was last saved.  I dont know what this would be used for but hey, its here if you need it.  It doesnt actually load anything all it does is return a mysql date.
    
   $field_name = The name of the variable in the DB to be checked.
   
   $id_field = This can be one of uid/nid/tid/vid or you can add new id fields to the table structure and use those as well.
   
   $nid = This is the value of the nid/uid/tid/vid that you specified with $id_field.  If you specify id_field, you have to specify this as well otherwise it associates with ID 0 of whatever nid/uid/etc table name you specified which probably isnt the desired action.

RETURN VALUES:
   This returns a string in the form of "2008-01-01 00:00:00"
   
EXAMPLES

   $last_modified = queryable_variables_get_date('somevariable');
   Returns the date of when 'somevariable' was last saved.
   
   $last_modified = queryable_variables_get_date('somevariable', 'tid', 18);
   Returns the date of when 'somevariable' was last saved.  This one was associated with tid=18.

DEFINITION:
   function queryable_variables_get_type($field_name, $id_field = 'nid', $nid = 0)
   This returns the PHP Data type of the stored variable.  It will return one of array/object/string/boolean/etc.
   
   $field_name = The name of the variable in the DB to be checked.
   
   $id_field = This can be one of uid/nid/tid/vid or you can add new id fields to the table structure and use those as well.
   
   $nid = This is the value of the nid/uid/tid/vid that you specified with $id_field.  If you specify id_field, you have to specify this as well otherwise it associates with ID 0 of whatever nid/uid/etc table name you specified which probably isnt the desired action.

RETURN VALUES:
   Can be any string of: array/object/boolean/double/date/integer/string
   
EXAMPLES:
   queryable_variables_get_type('tellmewhatIam');
   This returns the PHP Data Type of the stored variable called 'tellmewhatIam'.
   
   queryable_variables_get_type('tellmewhatIam', 'nid' 143);
   Same as above only this one has an association.
   
DEFINITION: 
   function queryable_variables_get_id($field_name, $id_field = 'nid', $nid = 0)
   This gets the auto-incremented ID of the variable $field_name
   
   $field_name = The name of the variable in the DB to be checked.
   
   $id_field = This can be one of uid/nid/tid/vid or you can add new id fields to the table structure and use those as well.
   
   $nid = This is the value of the nid/uid/tid/vid that you specified with $id_field.  If you specify id_field, you have to specify this as well otherwise it associates with ID 0 of whatever nid/uid/etc table name you specified which probably isnt the desired action.
   
RETURN VALUES:
   integer of the ID in the {queryable_variables} table for this particular variable.
   
EXAMPLES:
   $id = queryable_variables_get_id('gimmesomething');
   Returns the ID of the variable gimmesomething that is in the DB.
   
   $id = queryable_variables_get_id('gimmesomething', 'nid' 223);
   Same as above only associated with nid=223
   
DEFINITION:
   function is_date($string)
   This function checks for dates in a string.  The string MUST be in one of 2 formats listed below in the examples.
   
RETURN VALUES:  Returns TRUE/FALSE

EXAMPLES:
   is_date('2008-02-01'); <-- Returns TRUE
   is_date('ssss 2008-02-01');  <-- Returns FALSE
   is_date('2008-02-01 02:33:10') <-- Returns TRUE

   
DEFINITION:
   function queryable_variables_get_elements($field_value, $type)
   This is NOT an API function.  It is used internally and should never be used by other modules.


